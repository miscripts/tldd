CXXFLAGS := -O -g
PREFIX := /usr/local

all: tldd

tldd: tldd.cc

install: tldd
	install -D tldd $(DESTDIR)$(PREFIX)/bin/tldd
	install -D tldd.1 $(DESTDIR)$(PREFIX)/share/man/man1/tldd.1

clean:
	rm -f tldd
